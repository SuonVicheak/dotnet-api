﻿namespace WebApp
{
    public class Book
    {
        public int No { get; set; }
        public string Title { get; set; }
        public int Pages { get; set; }
    }

    public class BookList: List<Book>
    {
        public BookList()
        {
            AddRange(new Book[]
            {
                new Book {No = 1, Title = "Learning English", Pages = 100},
                new Book {No = 2, Title = "Learning C#", Pages = 300}
            }) ;
        }
    }
}
