﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        //because book list is not global for the controller,
        //we have to use Singleton pattern
        private BookList _books;

        public BooksController(BookList books) 
        {
            _books = books; 
        }

        [HttpGet("allBooks")]
        public List<Book> GetAll()
        {
            return _books.ToList();
        }

        [HttpPost("create")]
        public string Create(Book book)
        {
            _books.Add(book);
            return "Successfully"; 
        }
    }
}
